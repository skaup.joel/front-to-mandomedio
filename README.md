This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

In this application you can keep track of the pokemones you like <3

Enter your name at the beginning of the application, select some pokemones and look at them or remove them in your profile, you can also see the pokemones selected by other people using the navigation above in the app.

## Available Scripts

### `0) You must install nodejs on your computer`

In the project directory, you can run:

### `1) run: npm install`

To install the dependencies

### `2) run: npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
