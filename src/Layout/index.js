import React, {useState} from "react";
import {
    Router,
    Link,
    Switch,
    Route
  } from "react-router-dom";
  import history from './history';

import AllPokeLovers from '../pages/AllPokeLovers'
import Login from '../pages/Login'
import Perfil from '../pages/Perfil'
import AllUsers from '../pages/AllUsers'

export const Layout = ()=>{
    const [userId, setUserId] = useState('default');

    return (
        <div style={{height:"100vh"}}>
            <Router history={history}>
            <div style={{height:'5%',backgroundColor:'pink',
            flexDirection: 'row',
            display: 'flex',
            justifyContent: 'space-around',
            alignItems:'center'
        }}>
                <div>
                    <Link to={`/AllPoke/${userId}`}>
                        <span style={{fontSize:'.7em'}}>All Poke</span>
                    </Link>
                </div>
                <div>
                    <Link to="/AllUsers">
                        <span style={{fontSize:'.7em'}}>All Users</span>
                    </Link>
                </div>
                <div>
                    <Link to="/Login">
                        <span style={{fontSize:'.7em'}}>Change User</span>
                    </Link>
                </div>
                <div>
                    <Link to={`/Perfil/${userId}`}>
                        <span style={{fontSize:'.7em'}}>Perfil</span>
                    </Link>
                </div>
            </div>
            <div style={{height:'95%',backgroundColor:'blue', overflow: 'auto'}}>
                <Switch>
                <Route exact path="/" component={(props)=><Login {...props} getUser={(userId)=>setUserId(userId)}/>} />
                <Route exact path="/AllPoke/:userId" component={AllPokeLovers}/>
                <Route exact path="/Login" component={(props)=><Login {...props} getUser={(userId)=>setUserId(userId)}/>}/> 
                <Route exact path="/Perfil/:userId" component={Perfil}/> 
                <Route exact path="/AllUsers" component={AllUsers}/>
                </Switch>
            </div>
          
            </Router>
        </div>
    )
}