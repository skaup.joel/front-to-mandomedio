import React, { Component } from 'react'
import axios from 'axios'

import { PokeCard } from '../../components/PokeCard'
export default class AllPokeLovers extends Component {
    state={
        pokemons: null,
        userId:this.props.match.params.userId
    } 
    async componentDidMount(){
        const { data } = await axios.get('http://localhost:3001/getPokemons', {
            headers: { 
                'Content-Type': 'application/json', 
               }
        })
        this.setState({pokemons:data})
        
    }

    updatePokeLoved = async (data) => {
        axios({
            method: 'put',
            url: 'http://localhost:3001/updatePokeLoved',
            data: {
                pokeLoved: {...data},
                id:this.state.userId
                }
          })
    }

    render(){
        const { pokemons } = this.state;
        return(
            <div style={{height:"100%"}} >
                <h1 className='text-center'>All PokeLovers</h1>
                <div style={{flexDirection:'row', display:'flex', flexWrap:'wrap', justifyContent:'center'}} >
                {
                    pokemons && 
                    pokemons.results.map(pokemonData => 
                        <div style={{padding:'10px'}}>
                    <PokeCard key= {pokemonData.name} fnAddCard={this.updatePokeLoved} {...pokemonData} />
                        </div>
                    )
                }
                </div>
            </div>
        )
    }
}
