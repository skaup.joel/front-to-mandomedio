import React, { Component } from 'react'
import { Input as InputLogin } from '../../components/Input'
import history from './../../Layout/history';
import axios from 'axios'
export default class Login extends Component {
    start = async (input) => {
        axios({
            method: 'post',
            url: 'http://localhost:3001/createUser',
            data: {
                name:input,
                pokeLoved:[]
                }
          }).then(({data}) => {
            this.props.getUser(data._id)
            history.push(`/AllPoke/${data._id}`)})  
    }
    render(){
        
        return(
            <div className="d-flex flex-column justify-content-center align-items-center" style={{height:"100%"}} >
                <div style={{width:'25em'}}>
                    <InputLogin fnStart={this.start} />
                </div>
            </div>
        )
    }
}
