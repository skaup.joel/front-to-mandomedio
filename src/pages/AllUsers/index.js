import React, { Component } from 'react'
import axios from 'axios'

import { PokeCard } from '../../components/PokeCard'
export default class AllPokeLovers extends Component {
    state={
        data: null
    } 
    async componentDidMount(){
        const { data } = await axios.get('http://localhost:3001/getUsers', {
            headers: { 
                'Content-Type': 'application/json', 
               }
        })
        this.setState({data})
        
    }
    render(){
        const { data } = this.state;
        
        if(data) {
            return (<div style={{height:"100%"}} >
        <div style={{flexDirection:'row', display:'flex', flexWrap:'wrap', justifyContent:'center'}} >
        {
            data && data.map(
                data =>
            <div style={{width:'100%'}}>
               <h1>Pokemons de {data.name}:</h1>
               <div style={{display:'flex'}}>
                   {
                       data.pokeLoved.map(data=> <PokeCard withoutBtns {...data} /> )
                   }
               </div>
            </div>
            )
        }
        </div>
        </div>)}
        else{
            return(<h1>Cargando</h1>)
        }
        
    }
}
