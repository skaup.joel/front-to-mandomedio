import React, { Component } from 'react'
import axios from 'axios'
import { PokeCard } from '../../components/PokeCard'

export default class Perfil extends Component {
    state={
        pokemons: null,
        userId:this.props.match.params.userId,
        userName:''
    } 
    async componentDidMount(){
        this.getPokeUser()
    }
    getPokeUser = async() => {
        const {userId} = this.state
        const { data } = await axios.get(`http://localhost:3001/user/${userId}`, {
            headers: { 
                'Content-Type': 'application/json', 
               }
        })
        this.setState({pokemons:data.pokeLoved, userName:data.name})
    }
    removePokeLoved = async (data) => {
        axios({
            method: 'put',
            url: 'http://localhost:3001/removePokeLoved',
            data: {
                pokeLoved: {name:data},
                id:this.state.userId
                }
          }).then(()=>this.getPokeUser())
    }
    render(){
        const { pokemons } = this.state;
        return(
            <div className="d-flex flex-column justify-content-around align-items-center" style={{height:"100%"}} >
                <div>
                <h1>Tu Perfil</h1>
                <h3>Aqui !! Puedes ver tus pokemons favoritos </h3>
                </div>
                <div style={{flexDirection:'row', display:'flex', flexWrap:'wrap', justifyContent:'center'}} >
                {
                    pokemons && 
                    pokemons.map(pokemonData => 
                        <div style={{padding:'10px'}}>
                    <PokeCard key={pokemonData.name} withRemove fnAddCard={this.removePokeLoved} {...pokemonData} />
                        </div>
                    )
                }
                </div>
            </div>
        )
    }
}
