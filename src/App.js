import React from 'react';
import {Layout} from './Layout';

function App() {
  return (
    <div className="App" style={{backgroundColor:"red", height:"100vh"}}>
    <Layout/>
    </div>
  );
}

export default App;
