import React, {useState} from 'react'

export const Input = ({fnStart})=> {
    const [inputText, setCount] = useState('');
        return(
            <div className="input-group input-group-lg">
            <div className="input-group-prepend">
                <span className="input-group-text" id="inputGroup-sizing-lg">@</span>
            </div>
            <input 
            onChange={(e)=> setCount(e.target.value)}
            placeholder='Insert Your Name' type="text" className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" />
            <div class="input-group-append">
                <button
                 className="btn btn-primary"
                 type="button"
                 id="inputGroupFileAddon04"
                 onClick={()=>fnStart(inputText)}
                 >
                     Start
                </button>
            </div>
            </div>
        )
}
