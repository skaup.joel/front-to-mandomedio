import React from 'react'

export const PokeCard = (props)=> {
        const { name, url, fnAddCard, withRemove = false, withoutBtns = false } = props;
        const pokemonIndex = url.split('/')[url.split('/').length - 2];
        const imageUrl = `https://github.com/PokeAPI/sprites/blob/master/sprites/pokemon/${pokemonIndex}.png?raw=true`;

        return(
            <div className="card" style={{width: "18rem"}}>
                <img src={imageUrl} className="card-img-top" alt="..." />
                <div className="card-body">
                <h5 className="card-title">{name}</h5>
                    {withoutBtns ?  null :withRemove 
                    ? 
                    <button 
                    onClick={()=>{fnAddCard(name)}}
                    href="#" className="btn btn-danger">Remove</button>
                    : 
                    <button 
                    onClick={()=>{fnAddCard({name, url})}}
                    href="#" className="btn btn-primary">Like</button>
                    
                    }
                </div>
            </div>
        )
}
